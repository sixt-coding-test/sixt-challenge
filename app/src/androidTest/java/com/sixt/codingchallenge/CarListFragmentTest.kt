package com.sixt.codingchallenge

import androidx.fragment.app.testing.FragmentScenario
import androidx.fragment.app.testing.launchFragmentInContainer
import androidx.lifecycle.Lifecycle
import androidx.navigation.NavController
import androidx.navigation.Navigation
import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.action.ViewActions.click
import androidx.test.espresso.assertion.ViewAssertions.matches
import androidx.test.espresso.matcher.ViewMatchers.isDisplayed
import androidx.test.espresso.matcher.ViewMatchers.withId
import androidx.test.ext.junit.runners.AndroidJUnit4
import com.sixt.codingchallenge.fragments.CarListFragment
import com.sixt.codingchallenge.fragments.vm.SharedCarViewModel
import com.sixt.codingchallenge.model.Car
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.Mockito.mock
import org.mockito.Mockito.verify
import org.mockito.MockitoAnnotations

@RunWith(AndroidJUnit4::class)
class CarListFragmentTest {


    val mockNavController = mock(NavController::class.java)
    lateinit var scenario : FragmentScenario<CarListFragment>
    lateinit var viewModel: SharedCarViewModel

    @Before
    fun setup() {
        MockitoAnnotations.initMocks(this)
        scenario = launchFragmentInContainer<CarListFragment>(themeResId = R.style.Theme_AppCompat_NoActionBar)
        scenario.onFragment {fragment ->
            Navigation.setViewNavController(fragment.requireView(), mockNavController)
            viewModel = fragment.viewModel()
        }
    }


    @Test
    fun NavigateToMapFragmentTest() {
        onView(withId(R.id.button_map)).perform(click())
        verify(mockNavController).navigate(R.id.action_CarListFragment_to_MapFragment)
    }
}