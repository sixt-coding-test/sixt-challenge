package com.sixt.codingchallenge


import androidx.fragment.app.testing.FragmentScenario
import androidx.fragment.app.testing.launchFragmentInContainer
import androidx.navigation.NavController
import androidx.navigation.Navigation
import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.action.ViewActions.click
import androidx.test.espresso.matcher.ViewMatchers.withId
import androidx.test.ext.junit.runners.AndroidJUnit4
import com.sixt.codingchallenge.fragments.MapsFragment
import com.sixt.codingchallenge.fragments.vm.SharedCarViewModel
import junit.framework.TestCase.assertEquals
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.Mockito.mock
import org.mockito.Mockito.verify
import org.mockito.MockitoAnnotations


@RunWith(AndroidJUnit4::class)
class MapFragmentTest {

    val mockNavController = mock(NavController::class.java)
    lateinit var scenario : FragmentScenario<MapsFragment>

    @Before
    fun setup() {
        MockitoAnnotations.initMocks(this)
        scenario = launchFragmentInContainer<MapsFragment>(themeResId = R.style.Theme_AppCompat_NoActionBar)
        scenario.onFragment { fragment ->
            Navigation.setViewNavController(fragment.requireView(), mockNavController)
        }
    }

    @Test
    fun NavigateToListFragmentTest() {
        onView(withId(R.id.button_list)).perform(click())
        verify(mockNavController).popBackStack()
    }
}