package com.sixt.codingchallenge.di

import com.sixt.codingchallenge.util.coroutine.CoroutinesContext
import org.koin.dsl.module

val coroutineModules = module {
    factory { CoroutinesContext() }
}