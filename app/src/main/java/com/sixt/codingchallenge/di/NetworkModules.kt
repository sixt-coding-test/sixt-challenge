package com.sixt.codingchallenge.di

import com.sixt.codingchallenge.network.helper.SixtNetworkHelper
import com.sixt.codingchallenge.network.service.SixtCarService
import com.squareup.moshi.Moshi
import org.koin.dsl.module

val networkModules = module {
    single { SixtNetworkHelper(get()).createHelperService() }
}