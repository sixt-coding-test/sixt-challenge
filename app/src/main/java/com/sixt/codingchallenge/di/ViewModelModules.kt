package com.sixt.codingchallenge.di
import com.sixt.codingchallenge.fragments.vm.SharedCarViewModel
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.dsl.module

val viewModelModules = module {
    viewModel { SharedCarViewModel(get(), get(), get()) }
}
