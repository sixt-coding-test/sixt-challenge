package com.sixt.codingchallenge.di

import com.sixt.codingchallenge.repository.CarRepository
import com.sixt.codingchallenge.util.coroutine.CoroutinesContext
import org.koin.dsl.module

val repositoryModules = module {
    factory { CarRepository(get()) }
}