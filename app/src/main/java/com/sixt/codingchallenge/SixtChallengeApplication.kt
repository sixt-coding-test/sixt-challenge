package com.sixt.codingchallenge

import androidx.multidex.MultiDexApplication
import com.sixt.codingchallenge.di.coroutineModules
import com.sixt.codingchallenge.di.networkModules
import com.sixt.codingchallenge.di.repositoryModules
import com.sixt.codingchallenge.di.viewModelModules
import org.koin.android.ext.koin.androidContext
import org.koin.android.ext.koin.androidLogger
import org.koin.core.context.startKoin
import java.util.*

class SixtChallengeApplication : MultiDexApplication() {

    override fun onCreate() {
        super.onCreate()

        startKoin {
            androidContext(this@SixtChallengeApplication)
            androidLogger()
            modules(networkModules, repositoryModules, viewModelModules, coroutineModules)
        }
    }
}