package com.sixt.codingchallenge.repository

import com.sixt.codingchallenge.model.Car
import com.sixt.codingchallenge.network.service.SixtCarService

open class CarRepository(private val sixtCarService: SixtCarService) {

   suspend fun getCars(): List<Car> {
        return sixtCarService.getCars()
    }
}