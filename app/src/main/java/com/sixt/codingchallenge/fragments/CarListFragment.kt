package com.sixt.codingchallenge.fragments

import android.os.Bundle
import android.view.View
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.sixt.codingchallenge.R
import com.sixt.codingchallenge.adapter.CarAdapter
import com.sixt.codingchallenge.extensions.bindTo
import com.sixt.codingchallenge.extensions.injectSharedViewModel
import com.sixt.codingchallenge.fragments.base.BaseFragment
import com.sixt.codingchallenge.fragments.vm.SharedCarViewModel
import kotlinx.android.synthetic.main.content_car_list.*
import kotlinx.android.synthetic.main.fragment_car_list.*
import kotlin.reflect.KClass

class CarListFragment : BaseFragment<SharedCarViewModel>() {

    override val layoutId: Int
        get() = R.layout.fragment_car_list
    override val viewModelClass: KClass<SharedCarViewModel>
        get() = SharedCarViewModel::class
    override fun viewModel() = injectSharedViewModel()

    private lateinit var adapter: CarAdapter

    override fun initViews(savedInstanceState: Bundle?) {
        setupRecyclerView()
        button_map.setOnClickListener { showMap() }
        viewModel.getCars()
    }

    private fun setupRecyclerView() {
        val linearLayoutManager: LinearLayoutManager = object : LinearLayoutManager(activity) {
            override fun getExtraLayoutSpace(state: RecyclerView.State): Int {
                return 300
            }
        }
        cars_recycler.layoutManager = linearLayoutManager
        adapter = CarAdapter()

        cars_recycler.adapter = adapter
    }

    override fun bindViewModelData() {

        viewModel.getCarsLiveData().bindTo(viewLifecycleOwner) { carList ->
            adapter.updateCars(carList)
        }

        viewModel.getErrorLiveData().bindTo(viewLifecycleOwner) { message ->
            error_message.text = getString(message)
            error_message.visibility = View.VISIBLE
        }
    }

    private fun showMap() {
        findNavController().navigate(R.id.action_CarListFragment_to_MapFragment)
    }
}