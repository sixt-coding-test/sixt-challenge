package com.sixt.codingchallenge.fragments.base

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModel
import kotlin.reflect.KClass

abstract  class BaseFragment<T: ViewModel>: Fragment() {

    protected lateinit var viewModel: T

    protected abstract val layoutId: Int

    abstract val viewModelClass: KClass<T>

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        viewModel = viewModel()
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(layoutId, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initViews(savedInstanceState)
        bindViewModelData()
    }

    protected open fun bindViewModelData() {}

    abstract fun viewModel(): T

    abstract fun initViews(savedInstanceState: Bundle?)
}