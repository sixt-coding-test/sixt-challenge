package com.sixt.codingchallenge.fragments.vm

import android.app.Application
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.sixt.codingchallenge.R
import com.sixt.codingchallenge.model.Car
import com.sixt.codingchallenge.repository.CarRepository
import com.sixt.codingchallenge.util.coroutine.CoroutinesContext
import kotlinx.coroutines.CoroutineExceptionHandler
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext

class SharedCarViewModel(
    app: Application,
    private val carRepository: CarRepository,
    private val coroutinesContext: CoroutinesContext
) : ViewModel() {

    private var errorLiveData = MutableLiveData<Int>()
    private val carsLiveData = MutableLiveData<List<Car>>()


    fun getCarsLiveData(): LiveData<List<Car>> = carsLiveData
    fun getErrorLiveData(): LiveData<Int> = errorLiveData

    fun getCars() {
        viewModelScope.launch(coroutinesContext.Main + handler) {
            val cars = withContext(coroutinesContext.IO) { carRepository.getCars() }
            if (cars.isNotEmpty())
                carsLiveData.postValue(cars)
            else
                errorLiveData.postValue(R.string.error_no_car)
        }
    }

    private val handler = CoroutineExceptionHandler { _, exception ->
        errorLiveData.postValue(R.string.error_generic)
    }

}