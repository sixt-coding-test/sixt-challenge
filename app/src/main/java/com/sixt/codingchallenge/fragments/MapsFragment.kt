package com.sixt.codingchallenge.fragments

import android.graphics.Bitmap
import android.graphics.Canvas

import android.os.Bundle
import androidx.core.content.ContextCompat
import androidx.navigation.fragment.findNavController

import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.*
import com.sixt.codingchallenge.R
import com.sixt.codingchallenge.dialog.CarInfoBottomDialog
import com.sixt.codingchallenge.extensions.bindTo
import com.sixt.codingchallenge.extensions.injectSharedViewModel
import com.sixt.codingchallenge.fragments.base.BaseFragment
import com.sixt.codingchallenge.fragments.vm.SharedCarViewModel
import com.sixt.codingchallenge.model.Car
import kotlinx.android.synthetic.main.fragment_map.*
import kotlin.reflect.KClass


class MapsFragment : BaseFragment<SharedCarViewModel>() {

    override val layoutId: Int
        get() = R.layout.fragment_map

    override val viewModelClass: KClass<SharedCarViewModel>
        get() = SharedCarViewModel::class

    override fun viewModel() = injectSharedViewModel()

    private var googleMap: GoogleMap? = null

    private val callback = OnMapReadyCallback { googleMap ->
        this.googleMap = googleMap
        bindData()
    }

    override fun initViews(savedInstanceState: Bundle?) {
        initMap()
        button_list.setOnClickListener {
            findNavController().popBackStack()
        }
    }

    private fun initMap() {
        val mapFragment = childFragmentManager.findFragmentById(R.id.map) as SupportMapFragment?
        mapFragment?.getMapAsync(callback)
    }

    private fun bindData() {
        viewModel.getCarsLiveData().bindTo(viewLifecycleOwner) { carList ->
            val firstLocationToZoom = LatLng(carList[0].latitude!!, carList[0].longitude!!)
            for (car in carList) {
                googleMap?.addMarker(getMarker(car))?.tag = car
            }

            val cameraPosition =
                CameraPosition.Builder().target(firstLocationToZoom).zoom(11f).tilt(5f).build()
            googleMap?.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition))
        }

        this.googleMap?.setOnMarkerClickListener { marker ->
            val car = marker.tag as Car

            val dialog = CarInfoBottomDialog(requireContext())
            dialog.init(car)
            dialog.show()
            true
        }
    }


    private fun getMarker(car: Car): MarkerOptions {
        return MarkerOptions()
            .position(LatLng(car.latitude!!, car.longitude!!))
            .title(car.modelName)
            .icon(bitmapDescriptorFromVector(R.drawable.ic_car))
    }

    private fun bitmapDescriptorFromVector(vectorResId: Int): BitmapDescriptor? {
        return ContextCompat.getDrawable(requireContext(), vectorResId)?.run {
            setBounds(0, 0, intrinsicWidth, intrinsicHeight)
            val bitmap =
                Bitmap.createBitmap(intrinsicWidth, intrinsicHeight, Bitmap.Config.ARGB_8888)
            draw(Canvas(bitmap))
            BitmapDescriptorFactory.fromBitmap(bitmap)
        }
    }


}