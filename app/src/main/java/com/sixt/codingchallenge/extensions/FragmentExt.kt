package com.sixt.codingchallenge.extensions

import androidx.lifecycle.ViewModel
import com.sixt.codingchallenge.fragments.base.BaseFragment
import org.koin.androidx.viewmodel.ext.android.getSharedViewModel
import org.koin.androidx.viewmodel.ext.android.getViewModel

inline fun <reified T : ViewModel> BaseFragment<T>.injectSharedViewModel(): T {
    return getSharedViewModel(clazz = viewModelClass)
}

inline fun <reified T : ViewModel> BaseFragment<T>.injectViewModel(): T {
    return getViewModel(clazz = viewModelClass)
}