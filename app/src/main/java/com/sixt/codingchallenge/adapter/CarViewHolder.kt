package com.sixt.codingchallenge.adapter

import android.view.View
import com.bumptech.glide.Glide
import com.bumptech.glide.load.resource.drawable.DrawableTransitionOptions
import com.sixt.codingchallenge.R
import com.sixt.codingchallenge.adapter.base.BaseViewHolder
import com.sixt.codingchallenge.model.Car
import com.sixt.codingchallenge.util.Constant.TRANSMISSION_MANAUL
import kotlinx.android.synthetic.main.view_holder_car.view.*

class CarViewHolder(private val view: View) : BaseViewHolder<Car>(view) {
    override fun bind(item: Car) {
        if (item.transmission == TRANSMISSION_MANAUL) {
            view.transmission_text.text = view.context.getString(R.string.car_transmission_manual)
            view.ic_transmission.setImageResource(R.drawable.ic_gearbox_manual)
        } else {
            view.transmission_text.text = view.context.getString(R.string.car_transmission_auto)
            view.ic_transmission.setImageResource(R.drawable.ic_gearbox_automatic)
        }

        val make = item.make
        val model = item.modelName
        view.model_name_text.text = view.context.getString(R.string.car_make_model, make, model)
        view.driver_text.text = item.name
        view.license_plate_text.text = item.licensePlate
        view.gas_level_text.text = item.fuelLevel.toString()

        loadImage(item.carImageUrl.orEmpty())
    }

    private fun loadImage(url: String) {
        Glide.with(view)
            .load(url)
            .transition(DrawableTransitionOptions.withCrossFade())
            .error(R.drawable.ic_car_placeholder)
            .into(view.car_image)
    }

}

