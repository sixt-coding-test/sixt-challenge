package com.sixt.codingchallenge.adapter

import android.view.View
import androidx.recyclerview.widget.DiffUtil
import com.sixt.codingchallenge.R
import com.sixt.codingchallenge.adapter.base.BaseAdapter
import com.sixt.codingchallenge.model.Car


class CarAdapter : BaseAdapter<Car, CarViewHolder>() {

    override fun viewHolder(itemView: View) = CarViewHolder(itemView)

    override val layoutId: Int
        get() = R.layout.view_holder_car

    fun updateCars(cars: List<Car>) {
       // val diffResult = DiffUtil.calculateDiff(CarCallback(this.items, cars))
        val oldList = this.items
        val diffResult = DiffUtil.calculateDiff(object: DiffUtil.Callback() {
            override fun getOldListSize() = oldList.size

            override fun getNewListSize() = cars.size

            override fun areItemsTheSame(oldPosition: Int, newPosition: Int): Boolean {
                return oldList[oldPosition].id == cars[newPosition].id
            }

            override fun areContentsTheSame(oldPosition: Int, newPosition: Int): Boolean {
                return oldList[oldPosition] == cars[newPosition]
            }
        })
        this.items = cars
        diffResult.dispatchUpdatesTo(this)
    }
}