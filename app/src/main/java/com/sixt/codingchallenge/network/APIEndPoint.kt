package com.sixt.codingchallenge.network

object APIEndPoint {
    const val GET_SIXT_CARS = "codingtask/cars"
}