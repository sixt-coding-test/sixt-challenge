package com.sixt.codingchallenge.network.helper

import android.content.Context
import com.sixt.codingchallenge.network.service.SixtCarService
import com.sixt.codingchallenge.util.Constant.BASE_URL
import com.squareup.moshi.Moshi
import okhttp3.logging.HttpLoggingInterceptor

class SixtNetworkHelper (context: Context
):BaseNetworkHelper<SixtCarService>(context, SixtCarService::class.java) {

    override fun createHelperService(): SixtCarService {
        return createHelper(BASE_URL)
    }
}