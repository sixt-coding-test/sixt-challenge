package com.sixt.codingchallenge.network.service

import com.sixt.codingchallenge.model.Car
import com.sixt.codingchallenge.network.APIEndPoint
import retrofit2.http.GET

interface SixtCarService {

    @GET(APIEndPoint.GET_SIXT_CARS)
    suspend fun getCars(): List<Car>
}