package com.sixt.codingchallenge.activity

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.sixt.codingchallenge.R

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

    }
}