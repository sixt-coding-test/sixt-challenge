package com.sixt.codingchallenge.util

object Constant {
    val BASE_URL = "https://cdn.sixt.io"
    val CONNECTION_TIMEOUT_SEC = 60L
    val READ_TIMEOUT_SEC = 60L
    val TRANSMISSION_MANAUL = "M"
}