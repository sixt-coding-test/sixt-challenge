package com.sixt.codingchallenge.model



data class Car(

	val modelIdentifier: String? = null,
	val fuelLevel: Double? = null,
	val color: String? = null,
	val latitude: Double? = null,
	val modelName: String? = null,
	val transmission: String? = null,
	val licensePlate: String? = null,
	val fuelType: String? = null,
	val carImageUrl: String? = null,
	val series: String? = null,
	val innerCleanliness: String? = null,
	val name: String? = null,
	val id: String? = null,
	val make: String? = null,
	val group: String? = null,
	val longitude: Double? = null
)
