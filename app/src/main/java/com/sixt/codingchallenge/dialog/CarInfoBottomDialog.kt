package com.sixt.codingchallenge.dialog

import android.content.Context
import android.view.View
import com.bumptech.glide.Glide
import com.bumptech.glide.load.resource.drawable.DrawableTransitionOptions
import com.google.android.material.bottomsheet.BottomSheetDialog
import com.sixt.codingchallenge.R
import com.sixt.codingchallenge.model.Car
import com.sixt.codingchallenge.util.Constant
import kotlinx.android.synthetic.main.view_holder_car.view.*

class CarInfoBottomDialog(context: Context) : BottomSheetDialog(context) {

    fun init(car: Car) {
        val view = layoutInflater.inflate(R.layout.dialog_car_info_map, null)
        setContentView(view)
        setCancelable(true)
        if (car.transmission == Constant.TRANSMISSION_MANAUL) {
            view.transmission_text.text = view.context.getString(R.string.car_transmission_manual)
            view.ic_transmission.setImageResource(R.drawable.ic_gearbox_manual)
        } else {
            view.transmission_text.text = view.context.getString(R.string.car_transmission_auto)
            view.ic_transmission.setImageResource(R.drawable.ic_gearbox_automatic)
        }

        val make = car.make
        val model = car.modelName
        view.model_name_text.text = view.context.getString(R.string.car_make_model, make, model)
        view.driver_text.text = car.name
        view.license_plate_text.text = car.licensePlate
        view.gas_level_text.text = car.fuelLevel.toString()

        loadImage(view, car.carImageUrl.orEmpty())
    }

    private fun loadImage(view: View, url: String) {
        Glide.with(view)
            .load(url)
            .transition(DrawableTransitionOptions.withCrossFade())
            .error(R.drawable.ic_car_placeholder)
            .into(view.car_image)
    }
}