package com.sixt.codingchallenge.coroutine

import com.sixt.codingchallenge.util.coroutine.CoroutinesContext
import kotlinx.coroutines.Dispatchers
import kotlin.coroutines.CoroutineContext

class TestCoroutinesContext : CoroutinesContext() {
    override val IO: CoroutineContext = Dispatchers.Unconfined
    override val Main: CoroutineContext = Dispatchers.Unconfined
}