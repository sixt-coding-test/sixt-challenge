package com.sixt.codingchallenge

import android.app.Application
import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.lifecycle.Observer
import com.sixt.codingchallenge.coroutine.BaseCoroutineTestRule
import com.sixt.codingchallenge.coroutine.TestCoroutinesContext
import com.sixt.codingchallenge.fragments.vm.SharedCarViewModel
import com.sixt.codingchallenge.model.Car
import com.sixt.codingchallenge.network.service.SixtCarService
import com.sixt.codingchallenge.repository.CarRepository
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import org.junit.runners.JUnit4
import org.mockito.InjectMocks
import org.mockito.Mock

import org.mockito.Mockito
import org.mockito.Mockito.`when`
import org.mockito.Mockito.verify
import org.mockito.MockitoAnnotations


class SixtTest {

    @Mock
    private lateinit var app: Application

    // Mocks
    @InjectMocks
    private lateinit var repository: CarRepository

    @Mock
    private lateinit var sixtCarService: SixtCarService
    @Mock
    private lateinit var successObserver: Observer<List<Car>>
    @Mock
    private lateinit var errorObserver: Observer<Int>


    //Rules
    @get:Rule
    val baseCoroutineTestRule = BaseCoroutineTestRule()

    @get:Rule
    val instantExecutorRule = InstantTaskExecutorRule()

    private lateinit var sharedCarViewModel: SharedCarViewModel

    @Before
    fun setUp() {
        MockitoAnnotations.initMocks(this)
        sharedCarViewModel = SharedCarViewModel(
            app,
            repository,
            TestCoroutinesContext()
        ).apply {
            getCarsLiveData().observeForever(successObserver)
            getErrorLiveData().observeForever(errorObserver)
        }
    }

    @Test
    fun `Fetch and Display list of Cars` () = baseCoroutineTestRule.runBlockingTest {
        val cars = listOf(Car(), Car())
        `when`(repository.getCars()).thenReturn(cars)
        sharedCarViewModel.getCars()
        verify(successObserver).onChanged(cars)
    }

    @Test
    fun `Fetch cars and Display error if no car found` () = baseCoroutineTestRule.runBlockingTest {
        val cars = listOf<Car>()
        `when`(repository.getCars()).thenReturn(cars)
        sharedCarViewModel.getCars()
        verify(errorObserver).onChanged(R.string.error_no_car)
    }

    @Test
    fun `Fetch cars and Display generic Error if API fails` () = baseCoroutineTestRule.runBlockingTest {
        `when`((repository.getCars())).thenThrow(Error())
        sharedCarViewModel.getCars()
        verify(errorObserver).onChanged(R.string.error_generic)
    }
}