Sixt Coding Challenge
=============================

# App Design and language details:
* Language: Kotlin
* Dependency Injection: Koin
* Async Handling: Coroutine
* Navigation: Android Navigation Architecture
* Design Pattern: MVVM
* Reactive UI: LiveData observables
* Network: Retrofit, GSON
* Unit Test: Mockito, Expresso, Coroutine Test

# UI

* ConstraintLayout
* RecyclerView
* CardView

# libraries:

* Coroutine - Aysnc Handling 
* Koin - DI
* Retrofit- Network Operations
* Gson - POJO Operation
* Glide - Image Loading
* Google Maps SDK - Map Operations
* Mokito - Unit Test
* Expresso - 


# Working APK:
* https://drive.google.com/file/d/1uwXgwO6CrE74ycl7EGaGqA87mdVcG9jk/view?usp=sharing

